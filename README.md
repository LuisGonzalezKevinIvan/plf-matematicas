# 1. Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .nodoPrin {
    BackgroundColor #27AA80
  }
  .nodoSec {
    BackgroundColor #32FF6A
  }
  .nodoTer {
    BackgroundColor #A8FF3E
  }
  .nodoCuar {
    BackgroundColor #F4FF61 
  }
  .nodoQuin {
    BackgroundColor #96F7D2
  }
  .nodoSex {
    BackgroundColor #F0F696
  }
}
</style>
* Matematicas<<nodoPrin>>
	* Conjuntos<<nodoSec>>
		*_ ¿Qué es?
			* Es una colección de elementos.<<nodoCuar>>
		* Inclusión de conjuntos<<nodoTer>>
			*_ Es
				* La noción que hay entre dos conjuntos que \nes un trasunto de la relación de orden.<<nodoCuar>>
			* Todos los elementos del primero pertenecen al segundo.<<nodoCuar>>
				* Un conjunto está incluido en otro.<<nodoQuin>>
		* Operaciones<<nodoTer>>
			*_ Son
				* Conjuntos más complejos armados \nsobre los conjuntos elementales.<<nodoCuar>>
			* Basicas<<nodoCuar>>
				* Intersección<<nodoQuin>>
					* Elementos que pertenecen simultáneamente a ambos.<<nodoSex>>
				* Unión<<nodoQuin>>
					* Elementos que pertenecen a alguno de ellos.<<nodoSex>>
				* Complementación<<nodoQuin>>
					* Elementos que no pertenecen a un conjunto dado.<<nodoSex>>
				* Diferencia<<nodoQuin>>
					* Elementos del primer conjunto sin \nlos elementos del segundo.<<nodoSex>>
		*_ Tipos
			* Universal<<nodoCuar>>
				* Conjunto de referencia donde ocurre toda la teoría.<<nodoQuin>>
			* Vacio<<nodoCuar>>
				* Una necesidad lógica para cerrar bien las cosas.<<nodoQuin>>
				* Conjunto que no tiene elemento.<<nodoQuin>>
		* Representación gráfica<<nodoTer>>
			* Diagrama de Venn<<nodoCuar>>
				*_ Creador 
					* John Venn
				* Comprender la posición de estas operación.<<nodoQuin>>
				* Útil hacerse idea de como hace las cosas.<<nodoQuin>>
				* Inspirarse para demostrar los resultados.<<nodoQuin>>
				* No es útil para demostración.<<nodoQuin>>
				*_ Se representa mediante
					* Circulos.<<nodoQuin>>
					* Óvalos.<<nodoQuin>>
					* líneas cerradas.<<nodoQuin>>	
						* Encierran a los elementos que forman el conjunto<<nodoSex>>
		* Cardinal de un conjunto<<nodoTer>>
			*_ Es
				* El número de elementos que conforman un conjunto.<<nodoCuar>>	
			*_ Propiedades
				* Fórmula para resolver<<nodoQuin>>
					* Unión de conjuntos<<nodoSex>>
						* La cardinalidad de una unión es igual al cardinal de uno de los \nconjuntos, el conjunto "A" más el cardinal del segundo conjunto "B" \nmenos el cardinal de la intersección.
				* Acotación de cardinales<<nodoQuin>>
					* Mayor que.<<nodoSex>>
					* Menor que.<<nodoSex>>
					* Igual que.<<nodoSex>>
	* Aplicacion<<nodoSec>>
		*_ Es
			* Es una transformación que convierte a cada uno de los \nelementos del conjunto en único elemento de un conjunto final.<<nodoTer>>
		* Tipos<<nodoTer>>
			* Inyectiva<<nodoCuar>>
				* Si dos elementos tienen la misma imagen \nson del mismo elemento.<<nodoQuin>>
			* Sobreyectiva<<nodoCuar>>
				* Si es idéntico.<<nodoQuin>>
			* Biyectiva<<nodoCuar>>
				* Si es inyectiva y sobreyectiva.<<nodoQuin>>
 	* Funcion<<nodoTer>>
	 	* Grafica de la función<<nodoCuar>>
			* Serie de puntos.<<nodoQuin>>
			* Parábolas.<<nodoQuin>>
			* Línea recta.<<nodoQuin>>
@endmindmap
```
# 2. Funciones (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .nodoPrin {
    BackgroundColor #27AA80
  }
  .nodoSec {
    BackgroundColor #32FF6A
  }
  .nodoTer {
    BackgroundColor #A8FF3E
  }
  .nodoCuar {
    BackgroundColor #F4FF61 
  }
  .nodoQuin {
    BackgroundColor #96F7D2
  }
  .nodoSex {
    BackgroundColor #F0F696
  }
}
</style>
* Funciones<<nodoPrin>>
	*_ Son
		* Transformaciones de un conjunto a otro.<<nodoTer>>
	*_ Representación gráfica
		* Representación cartesiana<<nodoTer>>
			*_ Creada por
				* René Descartes<<nodoCuar>>
			*_ Significa
				* Representar en un plano los valores \nde los conjuntos de números.<<nodoCuar>>
					* En un eje las de las imágenes.<<nodoQuin>>
					* En otro eje los valores de un conjunto \nde números reales.<<nodoQuin>>
	* Tipos<<nodoSec>>
		*_ Se divide en
			* Crecientes<<nodoTer>>
				*_ Cuando
					* Aumenta la variable independiente, de \nigual manera sus imágenes.<<nodoCuar>>
			* Decrecientes<<nodoTer>>
				*_ Es 
					* Cuando disminuyen la variable independiente.<<nodoCuar>>
	* Comportamiento<<nodoSec>>
		* Máximos<<nodoTer>>
			*_ Es cuando
				* La función pasa por su valor máximo.<<nodoCuar>>
		* Mínimos<<nodoTer>>
			*_ Cuando 
				* La función decrece hasta su valor mínimo.<<nodoCuar>>
		* Límite<<nodoTer>>
			*_ Cuando
				* Los valores de la función están cerca de sus imágenes.<<nodoCuar>>
				* Nos acercamos a valores de punto de interés.<<nodoCuar>>
	* Derivada<<nodoSec>>
		*_ Objetivo
			* Aproximar una función complicada \ncon una función más simple.<<nodoTer>>
		* Funciones lineales<<nodoTer>>
			*_ Es
				* Una función sencilla donde su gráfica son línea rectas.<<nodoCuar>>
		* Cálculos<<nodoTer>>
			* Derivada de un producto.<<nodoCuar>>
			* Derivada de un exponente.<<nodoCuar>>
	*_ Puede ser
		* Continua<<nodoTer>>
			* No produce saltos ni discontinuidades.<<nodoCuar>>
			* Son funciones manejables.<<nodoCuar>>
			* Tienen buen comportamiento.<<nodoCuar>>
		* Discontinua<<nodoTer>>
			* En algún punto existe un salto.<<nodoCuar>>
	*_ Tienen
		* Intervalo<<nodoTer>>
			*_ Es
				* Un trozo del rango de los \nvalores que se puede tomar.<<nodoCuar>>
		* Dominio<<nodoTer>>
			*_ Es
				* Un conjunto de elementos "x" \nde la variable independiente.<<nodoCuar>>
@endmindmap
```
# 3. La matemática del computador (2002)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .nodoPrin {
    BackgroundColor #27AA80
  }
  .nodoSec {
    BackgroundColor #32FF6A
  }
  .nodoTer {
    BackgroundColor #A8FF3E
  }
  .nodoCuar {
    BackgroundColor #F4FF61 
  }
  .nodoQuin {
    BackgroundColor #96F7D2
  }
  .nodoSex {
    BackgroundColor #F0F696
  }
}
</style>
* Matemática del computador<<nodoPrin>>
	* Matematicas<<nodoSec>>
		*_ Es
			* Una ciencia que parte de \nuna deducción lógica.<<nodoTer>>
		*_ Nos permite
			* Estudiar los valores abstractos \ncomo son los números.<<nodoTer>>
		* Son importantes para el desarrollo \nde los computadores.<<nodoTer>>
		*_ Ayudan a
			* Resolver los problemas cotidianos.<<nodoTer>>
		* Representacion interna<<nodoTer>>
			* Números enteros<<nodoCuar>>
				*_ Representación en
					* Magnitud signo.<<nodoQuin>>
					* Exceso.<<nodoQuin>>
					* Complemento a dos.<<nodoQuin>>
			*_ Nos indica
				* Como tranformar un paso de corriente de unos y \nceros en una serie de posiciones de memoria.<<nodoCuar>>
		* Aritmética finita<<nodoTer>>
			* Digitos significativos<<nodoCuar>>
				* Miden la precisión general relativa de un valor.<<nodoQuin>>
			* Truncamiento<<nodoCuar>>
				*_ Es
					* Cortar unas cuantas cifras a la \nderecha apartir de una dada.<<nodoQuin>>
					* Cortar un numero que \ntiene infinitos decimales.<<nodoQuin>>
			* Redondeo<<nodoCuar>>
				* Un truncamiento refinado.<<nodoQuin>>
				* Los errores cometidos sean los menos posibles.<<nodoQuin>>
				* Evitar errores con los numeros<<nodoQuin>>
		*_ Existen los numeros
			* Números abstractos o teóricos.<<nodoCuar>>
			* Números reales.<<nodoCuar>>
			* Numeros decimales.<<nodoCuar>>
			* Números fraccional.<<nodoCuar>>
	*	Sistemas de númeracion<<nodoSec>>
		* Binario<<nodoTer>>
			*_ Uso en
				* Computadores.<<nodoCuar>>
			*_ Se representa
				* Mediante numero "1" y "0".<<nodoCuar>>
			* Enlaza la lógica booleana.<<nodoCuar>>
			* Se pueden crear estructuras complejas<<nodoCuar>>
				*_ Ejemplo
					* Letras.<<nodoQuin>>
					* Imágenes.<<nodoQuin>>
					* Signos de puntuación.<<nodoQuin>>
					* Videos.<<nodoQuin>>
		* Octal<<nodoTer>>
			* Sistema de base ocho.<<nodoCuar>>
				*_ numeros:
					* 0,1,2,3,4,5,6,7<<nodoQuin>>
			*  No se usan otros signos, solo numeros.<<nodoCuar>>
			*_  En la computadora
				* Se utiliza octal en vez \ndel sistema hexadecimal.<<nodoQuin>>
		* Hexadecimal<<nodoTer>>
			* Tiene base 16.<<nodoCuar>>
			* Sistema posicional.<<nodoCuar>>
@endmindmap
```